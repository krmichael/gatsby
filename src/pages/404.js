import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import Layout from "../components/layout"

export default () => (
  <Layout>
    <SEO title={`Page not found`} />
    <h2>Sorry, page not found (404)</h2>
    <Link to="/">Go to home</Link>
  </Layout>
)

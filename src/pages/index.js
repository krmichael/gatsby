import React from "react"
import { graphql, Link } from "gatsby"
import { css } from "@emotion/core"
import Img from "gatsby-image"

import { rhythm } from "../utils/typography"
import SEO from "../components/seo"
import Layout from "../components/layout"

export default ({ data }) => {
  return (
    <Layout>
      <SEO title={`Home`} />

      <div>
        <h1
          css={css`
            display: inline-block;
            border-bottom: 1px solid;
          `}
        >
          Amazing Pandas Eating Things
        </h1>

        <h4>{data.allMarkdownRemark.totalCount} Posts</h4>

        <Img
          fixed={data.file.childImageSharp.fixed}
          alt={`My background Image`}
        />

        {data.allMarkdownRemark.edges.map(({ node }) => (
          <div key={node.id}>
            <Link to={node.fields.slug}>
              <h3
                css={css`
                  margin-bottom: ${rhythm(1 / 4)};
                `}
              >
                {node.frontmatter.title}
              </h3>

              <span
                css={css`
                  color: #bbb;
                `}
              >
                {node.frontmatter.date}
              </span>

              <p>{node.excerpt}</p>
            </Link>
          </div>
        ))}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }

    file(relativePath: { eq: "images/backgroundpc.png" }) {
      childImageSharp {
        fixed(width: 125, height: 125) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`
